FROM python:3.9-alpine

WORKDIR /app

COPY requirements.txt .
COPY main.py .

RUN pip install -r requirements.txt

EXPOSE 3000

CMD [ "python", "main.py" ]
