from flask import Flask, jsonify, request, Response
import os
import json
from dicttoxml import dicttoxml
import re


app = Flask(__name__)



@app.route("/api/environment")
def get_environment():
    environment = {}
    for key, value in os.environ.items():
        environment[key] = value

    # Получение цветов фона и текста из переменных среды
    bgcolor = os.environ.get("BGCOLOR", "FFFFFF")
    fgcolor = os.environ.get("FGCOLOR", "000000")

    # Проверка, являются ли значения цветов действительными шестнадцатеричными кодами цветов
    color_regex = re.compile(r"^#(?:[0-9a-fA-F]{3}){1,2}$")
    if not color_regex.match(bgcolor):
        bgcolor = "FFFFFF"
    if not color_regex.match(fgcolor):
        fgcolor = "000000"

    # определение параметра формата из запроса URL
    output_format = request.args.get("format", "html")

    # Вывод в формате HTML (по умолчанию)
    if output_format == "html":
        html_output = f"<body style='background-color:#{bgcolor}; color:#{fgcolor};'>"
        html_output += "<h1>Environment Variables:</h1><ul>"
        for key, value in environment.items():
            html_output += f"<li>{key} : {value}</li>"
        html_output += "</ul></body>"
        return html_output

    # Вывод в формате JSON
    elif output_format == "json":
        json_output = json.dumps(environment)
        return Response(json_output, content_type='application/json')

    # Вывод в формате XML
    elif output_format == "xml":
        xml_output = dicttoxml(environment, custom_root='environment', attr_type=False)
        return Response(xml_output, content_type='application/xml')

    # Если указан неверный формат, возвращаем ошибку 400 (неправильный запрос)
    else:
        return "Invalid output format specified.", 400

@app.route('/api/me')
def about_me():
    my_info = {
        'name': 'Илья',
        'age': 32,
        'location': 'Riga, Latvia',
        'occupation': 'Devops Engineer',
        'interests': ['Programming', 'Reading', 'Traveling', 'CI/CD', 'Development']
    }
    return jsonify(my_info)


@app.route("/api/headers")
def get_headers():
    headers = {}
    for key, value in request.headers.items():
        headers[key] = value

    # определение параметра формата из запроса URL
    output_format = request.args.get("format", "html")

    # Вывод в формате HTML (по умолчанию)
    if output_format == "html":
        html_output = "<h1>Request Headers:</h1><ul>"
        for key, value in headers.items():
            html_output += f"<li>{key} : {value}</li>"
        html_output += "</ul>"
        return html_output

    # Вывод в формате JSON
    elif output_format == "json":
        json_output = json.dumps(headers)
        return Response(json_output, content_type='application/json')

    # Вывод в формате XML
    elif output_format == "xml":
        xml_output = dicttoxml(headers, custom_root='headers', attr_type=False)
        return Response(xml_output, content_type='application/xml')

    # Если указан неверный формат, возвращаем ошибку 400 (неправильный запрос)
    else:
        return "Invalid output format specified.", 400


@app.route('/api/post', methods=['POST'])
def handle_post():
    if request.method != 'POST':
        return 'Method not allowed', 405

    # get data from request body
    data = request.get_json()

    # output the data in different formats
    output_format = request.args.get('format', 'json')
    if output_format == 'json':
        return jsonify(data)
    elif output_format == 'xml':
        xml_data = dicttoxml(data, custom_root='data', attr_type=False)
        return Response(xml_data, content_type='application/xml')
    else:
        return 'Invalid output format specified', 400


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000)
